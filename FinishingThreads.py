__author__ = 'mohammad'

import threading
import time

is_finishing = False

def process0():
    while is_finishing == False:
        print("00000")
        time.sleep(3)


def process1():
    while True:
        print("11111")
        if is_finishing:
            break
        time.sleep(3)


def process2():
    global is_finishing
    while True:
        print("22222")
        if is_finishing:
            break
        time.sleep(3)


def show_information():
    print("Show information: \n")


def finishing():
    global is_finishing
    is_finishing = True


def user_control():
    while True:
        key_input = input("")
        if key_input == '0':
            finishing()
        else:
            if key_input == '1':
                show_information()
            else:
                print("Your is invalid!!!! Try again!\n")


if __name__ == '__main__':


    process0_thread = threading.Thread(target=process0, args=(), name="thread_0")
    process1_thread = threading.Thread(target=process1, args=(), name="thread_1")
    process2_thread = threading.Thread(target=process2, args=(), name="thread_2")

    process0_thread.start()
    process1_thread.start()
    process2_thread.start()

    user_control_thread = threading.Thread(target=user_control)
    user_control_thread.start()



